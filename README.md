# 機器人範例

啟動它:

    bundle install
    rackup

送個請求:

    curl -X POST http://localhost:9292/webhook \
         -H 'Content-Type: application/json' \
         -d '{ "key": "key-given-from-service", 
               "target": "ジャパリパーク", 
               "nickname": "ayaya", 
               "body": "!echo HELLO", 
               "callback_url": "http://localhost:9292/callback" }'
