require 'sinatra'

Bundler.require

use Rack::PostBodyContentTypeParser

get '/' do
  'Hello World!'
end

count = 0

post '/webhook' do
  key, nickname, target, body, callback_path =
    params.values_at('key', 'nickname', 'target', 'body', 'callback_path')

  case body
  when /^!echo (.+)/
    BunyaService.say(key, callback_path, $1)

  when /^!roll\b(.*)?$/
    body = if $1 != ''
             "@#{nickname} **#{rand(1..100)}** # #{$1}"
           else
             "@#{nickname} **#{rand(1..100)}**"
           end
    BunyaService.say(key, callback_path, body)

  when /^!oda$/
    count += 1
    BunyaService.say(key, callback_path, "我被毆打了 #{count} 次 Q_Q")

  else
    # do nothing
  end
  { status: 'ok' }.to_json
end

post '/callback' do
  params.to_json
end

class BunyaService
  include HTTParty

  base_uri ENV['BUNYA_URL'] || 'http://bunya.test'

  def self.say(key, path, body)
    BunyaService.post(path,
                      body: { body: body },
                      headers: { 'X-Bunya-Key' => key }).body
  end
end
